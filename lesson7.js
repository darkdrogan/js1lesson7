/* eslint-disable no-undef,no-use-before-define */
// бесит is assigned and never used - используется в функциях и пиздит=)
// todo: самоубийство при быстром развороте - исправить
// или нет) теоретически дз сделано=) Практически, я бы тут правил и правил)

// Глобальные переменные:
const FIELD_SIZE_X = 20;
const FIELD_SIZE_Y = 20;
const SNAKE_SPEED = 300; // Интервал между перемещениями змейки
const snake = []; // Сама змейка
let direction = 'y+'; // Направление движения змейки
let gameIsRunning = false; // Запущена ли игра
let snakeTimer; // Таймер змейки
let foodTimer; // Таймер для еды
let score = 0; // Результат

function init() {
  prepareGameField(); // Генерация поля
  const wrap = document.getElementsByClassName('wrap')[0];
  // Подгоняем размер контейнера под игровое поле
  if (16 * (FIELD_SIZE_X + 1) < 380) {
    wrap.style.width = '380px';
  } else {
    wrap.style.width = `${(16 * (FIELD_SIZE_X + 1)).toString()}px`;
  }

  // События кнопок Старт и Новая игра
  document.getElementById('snake-start').addEventListener('click', startGame);
  document.getElementById('snake-renew').addEventListener('click', refreshGame);

  // Отслеживание клавиш клавиатуры
  addEventListener('keydown', changeDirection);
}

/**
 * Функция генерации игрового поля
 */
function prepareGameField() {
  // Создаём таблицу
  const gameTable = document.createElement('table');
  gameTable.setAttribute('class', 'game-table');

  // Генерация ячеек игровой таблицы
  for (let i = 0; i < FIELD_SIZE_Y; i += 1) {
    // Создание строки
    const row = document.createElement('tr');
    row.className = `game-table-row row-${i}`;

    for (let j = 0; j < FIELD_SIZE_X; j += 1) {
      // Создание ячейки
      const cell = document.createElement('td');
      cell.className = `game-table-cell cell-${i}-${j}`;

      row.appendChild(cell); // Добавление ячейки
    }
    gameTable.appendChild(row); // Добавление строки
  }

  document.getElementById('snake-field').appendChild(gameTable); // Добавление таблицы
}

/**
 * Старт игры
 */
function startGame() {
  gameIsRunning = true;
  respawn();

  updateScore();

  snakeTimer = setInterval(move, SNAKE_SPEED);
  setTimeout(createFood, 5);
  setTimeout(createAnotherObstacle, 1);
}

/**
 * Функция расположения змейки на игровом поле
 */
function respawn() {
  // очищает массив при повторном нажатии старт
  // а то у автора каждый раз увеличивается только)
  while (snake.length) {
    snake.pop();
  }
  // Змейка - массив td
  // Стартовая длина змейки = 2
  // TODO: clear field before start new game

  // ReSpawn змейки из центра
  const startCoordX = Math.floor(FIELD_SIZE_X / 2);
  const startCoordY = Math.floor(FIELD_SIZE_Y / 2);

  // Голова змейки
  const snakeHead = document.getElementsByClassName(`cell-${startCoordY}-${startCoordX}`)[0];
  snakeHead.setAttribute('class', `${snakeHead.getAttribute('class')} snake-unit`);
  // Тело змейки
  const snakeTail = document.getElementsByClassName(`cell-${startCoordY - 1}-${startCoordX}`)[0];
  snakeTail.setAttribute('class', `${snakeTail.getAttribute('class')} snake-unit`);

  snake.push(snakeHead);
  snake.push(snakeTail);
}

function createAnotherObstacle() {
  for (let i = 0; i < 100; i += 1) {
    // рандом
    const obstacleX = Math.floor(Math.random() * FIELD_SIZE_X);
    const obstacleY = Math.floor(Math.random() * FIELD_SIZE_Y);

    const obstacleCell = document.getElementsByClassName(`cell-${obstacleY}-${obstacleX}`)[0];
    const obstacleCellClasses = obstacleCell.getAttribute('class').split(' ');

    // проверка на змейку
    if (!obstacleCellClasses.includes('snake-unit')) {
      let classes = '';
      for (let j = 0; j < obstacleCellClasses.length; j += 1) {
        classes += `${obstacleCellClasses[j]} `;
      }

      obstacleCell.setAttribute('class', `${classes}obstacle-unit`);
    }
  }
}

function isObstacle(unit) {
  let check = false;

  const unitClasses = unit.getAttribute('class').split(' ');

  // Если еда
  if (unitClasses.includes('obstacle-unit')) {
    check = true;
  }
  return check;
}

/**
 * Движение змейки
 */
function move() {
  // console.log('move',direction);
  // Сборка классов
  const snakeHeadClasses = snake[snake.length - 1].getAttribute('class').split(' ');

  // Сдвиг головы
  let newUnit;
  const snakeCoords = snakeHeadClasses[1].split('-');
  let coordY = parseInt(snakeCoords[1]);
  let coordX = parseInt(snakeCoords[2]);

  // Определяем новую точку - author
  // мне очень лень сейчас полностью прописывать правило на изменение кординат,
  // поэтому сделаю не красиво,
  // а топорно, но работать будет
  if (direction === 'x-') {
    if (coordX - 1 < 0) {
      coordX = FIELD_SIZE_X;
    }
    newUnit = document.getElementsByClassName(`cell-${coordY}-${coordX - 1}`)[0];
  } else if (direction === 'x+') {
    if (coordX + 1 > FIELD_SIZE_X - 1) {
      coordX = -1;
    }
    newUnit = document.getElementsByClassName(`cell-${coordY}-${coordX + 1}`)[0];
  } else if (direction === 'y+') {
    if (coordY - 1 < 0) {
      coordY = FIELD_SIZE_Y;
    }
    newUnit = document.getElementsByClassName(`cell-${coordY - 1}-${coordX}`)[0];
  } else if (direction === 'y-') {
    if (coordY + 1 > FIELD_SIZE_Y - 1) {
      coordY = -1;
    }
    newUnit = document.getElementsByClassName(`cell-${coordY + 1}-${coordX}`)[0];
  }

  // Проверки
  // 1) newUnit не часть змейки
  // 2) Змейка не ушла за границу поля
  // console.log(newUnit);
  if (!isSnakeUnit(newUnit) && newUnit !== undefined) {
    // Добавление новой части змейки
    newUnit.setAttribute('class', `${newUnit.getAttribute('class')} snake-unit`);
    snake.push(newUnit);
    if (isObstacle(newUnit)) {
      finishTheGame();
    }
    // Проверяем, надо ли убрать хвост
    if (!haveFood(newUnit)) {
      // Находим хвост
      const removed = snake.splice(0, 1)[0];
      const classes = removed.getAttribute('class').split(' ');

      // удаляем хвост
      removed.setAttribute('class', `${classes[0]} ${classes[1]}`);
    }
  } else {
    finishTheGame();
  }
}

/**
 * Проверка на змейку
 * @param unit
 * @returns {boolean}
 */
function isSnakeUnit(unit) {
  return snake.includes(unit);
}

function updateScore(currentScore = -1) {
  const scoreField = document.getElementById('score');
  score = currentScore + 1;
  scoreField.innerText = `Ваш счет: ${score}`;
}

/**
 * проверка на еду
 * @param unit
 * @returns {boolean}
 */
function haveFood(unit) {
  let check = false;

  const unitClasses = unit.getAttribute('class').split(' ');

  // Если еда
  if (unitClasses.includes('food-unit')) {
    check = true;
    createFood();
    updateScore(score);
  }
  return check;
}

/**
 * Создание еды
 */
function createFood() {
  let foodCreated = false;

  while (!foodCreated) {
    // рандом
    const foodX = Math.floor(Math.random() * FIELD_SIZE_X);
    const foodY = Math.floor(Math.random() * FIELD_SIZE_Y);

    const foodCell = document.getElementsByClassName(`cell-${foodY}-${foodX}`)[0];
    const foodCellClasses = foodCell.getAttribute('class').split(' ');

    // проверка на змейку
    if (!foodCellClasses.includes('snake-unit') && !foodCellClasses.includes('obstacle-unit')) {
      let classes = '';
      for (let i = 0; i < foodCellClasses.length; i += 1) {
        classes += `${foodCellClasses[i]} `;
      }

      foodCell.setAttribute('class', `${classes}food-unit`);
      foodCreated = true;
    }
  }
}

/**
 * Изменение направления движения змейки
 * @param e - событие
 */
function changeDirection(e) {
  switch (e.keyCode) {
    case 37: // Клавиша влево
      if (direction !== 'x+') {
        direction = 'x-';
      }
      break;
    case 38: // Клавиша вверх
      if (direction !== 'y-') {
        direction = 'y+';
      }
      break;
    case 39: // Клавиша вправо
      if (direction !== 'x-') {
        direction = 'x+';
      }
      break;
    case 40: // Клавиша вниз
      if (direction !== 'y+') {
        direction = 'y-';
      }
      break;
    default:
      break;
  }
}

/**
 * Функция завершения игры
 */
function finishTheGame() {
  gameIsRunning = false;
  clearInterval(snakeTimer);
  alert(`Вы проиграли! Ваш результат: ${score.toString()}`);
}

/**
 * Новая игра
 */
function refreshGame() {
  location.reload();
}

// Инициализация
window.onload = init;
